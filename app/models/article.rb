class Article < ApplicationRecord
    extend Mobility
    translates :name, locale_accessors: [:en, :ja]
end
#rails generate mobility:translations article name:string --backend=table