
class PostType < ApplicationRecord
    # extend Mobility
    # translates :name, backend: 'post_type_translations.rb'
    
    
   
    extend Mobility
    translates :name, fallbacks: { en: :ja, ja: :en }

end


PostType.new(name_en: "foo").name_ja