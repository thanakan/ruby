json.extract! category, :id, :image, :sub_categories, :article_type_id, :created_at, :updated_at
json.url category_url(category, format: :json)
