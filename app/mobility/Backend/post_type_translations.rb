class postTypeTranslations
    include Mobility::Backend

    def translation_for(locale, _)
        translation = translations.find { |t| t.key == attribute && t.locale == locale.to_s }
        translation ||= translations.build(locale: locale, key: attribute)
        translation
    end

    def translations
        model.send(association_name)
    end
  
    def read(locale, **options)
        translation_for(locale, options).value
    end
  
    def write(locale, value, **options)
      # ...
    end
  
    def self.configure(options)
      # ...
    end
  
    def each_locale
        translations.each { |t| yield(t.locale.to_sym) if t.key == attribute }
    end

    def build_node(attr, locale)
        aliased_table = class_name.arel_table.alias(table_alias(attr, locale))
        Arel::Attribute.new(aliased_table, :value, locale, self, attribute_name: attr.to_sym)
    end
  
    def setup do |attributes, options|
        association_name   = options[:association_name]
        translations_class = options[:class_name]
      
        has_many association_name, ->{ where key: attributes },
          as: :translatable,
          class_name: translations_class.name,
          dependent:  :destroy,
          inverse_of: :translatable,
          autosave:   true
    end
  end