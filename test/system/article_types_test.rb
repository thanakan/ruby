require "application_system_test_case"

class ArticleTypesTest < ApplicationSystemTestCase
  setup do
    @article_type = article_types(:one)
  end

  test "visiting the index" do
    visit article_types_url
    assert_selector "h1", text: "Article Types"
  end

  test "creating a Article type" do
    visit article_types_url
    click_on "New Article Type"

    fill_in "Name", with: @article_type.name
    click_on "Create Article type"

    assert_text "Article type was successfully created"
    click_on "Back"
  end

  test "updating a Article type" do
    visit article_types_url
    click_on "Edit", match: :first

    fill_in "Name", with: @article_type.name
    click_on "Update Article type"

    assert_text "Article type was successfully updated"
    click_on "Back"
  end

  test "destroying a Article type" do
    visit article_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article type was successfully destroyed"
  end
end
