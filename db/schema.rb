# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_11_091046) do

  create_table "article_translations", force: :cascade do |t|
    t.string "name"
    t.string "locale", null: false
    t.integer "article_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["article_id", "locale"], name: "index_article_translations_on_article_id_and_locale", unique: true
    t.index ["locale"], name: "index_article_translations_on_locale"
  end

  create_table "article_type_translations", force: :cascade do |t|
    t.string "name"
    t.string "locale", null: false
    t.integer "article_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["article_type_id", "locale"], name: "index_article_type_translations_on_article_type_id_and_locale", unique: true
    t.index ["locale"], name: "index_article_type_translations_on_locale"
  end

  create_table "articles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "image"
    t.integer "sub_categories"
    t.integer "article_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["article_type_id"], name: "index_categories_on_article_type_id"
  end

  create_table "categories_translations", force: :cascade do |t|
    t.string "locale"
    t.string "name"
    t.integer "categories_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["categories_id"], name: "index_categories_translations_on_categories_id"
  end

  create_table "post_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "article_translations", "articles"
  add_foreign_key "article_type_translations", "article_types"
  add_foreign_key "categories", "article_types"
  add_foreign_key "categories_translations", "categories", column: "categories_id"
end
