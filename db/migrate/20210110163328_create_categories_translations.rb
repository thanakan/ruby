class CreateCategoriesTranslations < ActiveRecord::Migration[6.1]
  def change
    create_table :categories_translations do |t|
      t.string :locale
      t.string :name
      t.references :categories, null: false, foreign_key: true

      t.timestamps
    end
  end
end
