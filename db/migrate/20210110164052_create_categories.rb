class CreateCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :categories do |t|
      t.string :image
      t.integer :sub_categories
      t.references :article_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
