class CreateArticleTypeNameTranslationsForMobilityTableBackend < ActiveRecord::Migration[6.1]
  def change
    create_table :article_type_translations do |t|

      # Translated attribute(s)
      t.string :name

      t.string  :locale, null: false
      t.references :article_type, null: false, foreign_key: true, index: false

      t.timestamps null: false
    end

    add_index :article_type_translations, :locale, name: :index_article_type_translations_on_locale
    add_index :article_type_translations, [:article_type_id, :locale], name: :index_article_type_translations_on_article_type_id_and_locale, unique: true

  end
end
