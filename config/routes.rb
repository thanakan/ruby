Rails.application.routes.draw do
  resources :post_types
  resources :categories
  resources :categories_translations
  resources :article_types_translations
  resources :article_types
  resources :articles
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
